from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),                          # The admin page
    url(r'^grappelli/', include('grappelli.urls')),                     # grappelli URLS
    url(r'^$', 'wahs.views.index', name="index"),                       # The index
    url(r'^crew/$', 'wahs.views.showCrew', name="crew"),                # The crew page 
    url(r'^crew/(?P<crewMember>\w+)/$', 'wahs.views.listMemberProductions', name="listMemberProductions"), # A page of all work a crew member is tagged in 
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))
