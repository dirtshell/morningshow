from django.db import models
from django import forms
from taggit.managers import TaggableManager

class CrewMember(models.Model):
    # Sections in order: Producers -> Web -> Reporters -> Advisors (Molina)
    CREW_POSITION_CHOICES = (
        ('Executive Producer', 'Executive Producer'),           # 2
        ('Coexecutive Producer', 'Coexecutive Producer'),       # NA
        ('Managing Producer', 'Managing Producer'),             # 1
        ('Crew Chief', 'Crew Chief'),                           # 3
        ('Executive Web Producer', 'Executive Web Producer'),   # NA
        ('Live-events Producer', 'Live-events Producer'),       # NA
        ('Sports Producer', 'Sports Producer'),                 # NA
        ('Entertainment Producer', 'Entertainment Producer'),   # NA
        ('World News Producer', 'World News Producer'),         # NA
        ('Technical Producer', 'Technical Producer'),           # NA
        ('Reporter', 'Reporter'),                               # NA
        ('Advisor', 'Advisor'),                                 # NA
        ('Web Manager', 'Web Manager'),                         # NA
    )
    
    firstName               =   models.CharField(max_length=20)
    lastName                =   models.CharField(max_length=20)
    position                =   models.CharField(max_length=50, choices=CREW_POSITION_CHOICES)
    profilePic              =   models.ImageField(upload_to='crewPics')
    bio                     =   models.CharField(max_length=600)

    def __unicode__(self):
        return "{0} {1}".format(self.firstName, self.lastName)  # This is how you format strings now
    
class Segment(models.Model):
    title                   =   models.CharField(max_length=50)
    authors                 =   models.ForeignKey('CrewMember')
    
    def __unicode__(self):
        return self.title
    
class Video(models.Model):
    title                   =   models.CharField(max_length=30)
    video                   =   models.FileField(upload_to='videos')
    author                  =   models.ForeignKey('CrewMember')
    segment                 =   models.ForeignKey('Segment')
    tags                    =   TaggableManager()
    creation_date           =   models.DateTimeField(auto_now_add = True)
    last_updated            =   models.DateTimeField(auto_now = True)
    activated               =   models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.title
        
class VideoForm(forms.Form):
    title                   =   forms.CharField(max_length=30)
    author                  =   forms.ModelChoiceField(queryset=CrewMember.objects.all(), empty_label=None)
    segment                 =   forms.ModelChoiceField(queryset=Segment.objects.all(), empty_label=None)
    tags                    =   forms.CharField(max_length=50)
