from django.contrib import admin
from wahs.models import *

admin.site.register(CrewMember)
admin.site.register(Segment)
admin.site.register(Video)