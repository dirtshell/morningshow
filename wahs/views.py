from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.template import RequestContext, loader
from django.shortcuts import render
import sys
import pdb
from wahs.models import *

def index(request):
    return render(request, 'index.html')

def showCrew(request):
    crewMembers = CrewMember.objects.filter()
    fullNames = []
    for member in crewMembers:
        fullNames.append("{0}_{1}".format(member.firstName, member.lastName))   # Combine the names for the proper URL format
    zippedData = zip(crewMembers, fullNames)    # Have to zip this to be able to access the fullnames alongside the members
    return render(request, 'crew.html', {
        'crewMembers' : zippedData,})
    
def showArchive(request):
    return render(request, 'archive.html')

def listMemberProductions(request, crewMember="Nobody"):
    name = crewMember.split('_')
    try:
        # Split the name into a first and last name for the query
        firstName = name[0].capitalize()
        lastName = name[1].capitalize()
        author = CrewMember.objects.get(firstName = firstName, lastName = lastName) # Need to have a error for in case the member does not exist
        productions = Video.objects.filter(author = author) # Need to have a message in case there are no productions associated with someone
    except:
        # If the name was too short
        return HttpResponse("OH NOES! Something seems to have gone wrong.") # This needs to return the error page
    #return HttpResponse(productions)
    return render(request, 'search.html', {
        'productions': productions,})

def addVideo(request):
    # Take the POST data, make sure it has a video, and then add the URL, and other details to the databasefrom the form
    # On failure
    # return render(request, 'fail.html')
    # On success
    if request.method == 'POST': # If the form has been submitted...
        form = VideoForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            video = Video()
            video.title = form.cleaned_data['title']
            video.author = form.cleaned_data['author']
            video.segment = form.cleaned_data['segment']
            # Take the tags string and seperate it by commas
            video.save()
            return render(request, 'success.html') # Redirect after POST
    else:
        form = VideoForm() # An unbound form

    return render(request, 'upload.html', {
        'form': form,
    })

# Dev function for testing video players    
def displayTestVideo(request):
    # Simply display the first video in the database
    vidList = Video.objects.filter(tags__name__in=["testing"])
    # Super fast way to grab the first entry in a queryset
    tempVidList = list(vidList[:1])
    if tempVidList:
        vid = tempVidList[0]
        return render(request, 'vidtesting.html', {
            'videoURL' : vid.video.name, 
            'videoName' : vid, })
    else:
        return render(request, 'ohnoes.html')
    
